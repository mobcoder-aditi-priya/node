//methods to make an object immutable

//object.preventExtension=>irreversible operaation
//prevents the addition of new properties to our existing object
let obj={
    name:"aditi",
    stream:"node",
    system:"dell"
}
console.log(Object.isExtensible(obj),"Extensiblitity"); // true
Object.preventExtensions(obj);
console.log(Object.isExtensible(obj),"Extensiblitity"); // false
obj.address = 'Chennai';
console.log(obj.address) // undefined


//Object.seal=>prevents additions or deletion of properties &modification of property descriptors.
console.log(Object.isSealed(obj),"sealed"); // false
Object.seal(obj);
console.log(Object.isSealed(obj),"sealed"); // true
obj.address = 'Mumbai';
console.log(obj.address); // undefined
delete obj.stream; // false
console.log(obj.stream); // node
//Object.defineProperty(obj, 'stream'); // TypeError: Cannot redefine property: stream


//Object.freeze=>does same that Object.seal() && makes the properties non-writable
console.log(Object.isFrozen(obj)," frozen"); // false
Object.freeze(obj);
console.log(Object.isFrozen(obj)," frozen"); // true
obj.address = 'gujrat';
console.log(obj.address); // undefined

delete obj.stream;
console.log(obj.stream); // node

//Object.defineProperty(obj, 'stream'); // TypeError: Cannot redefine property: stream

obj.stream = "android";
console.log(obj.stream); // node


//use strict mode=>to throw an error when trying to modify an immutable object








