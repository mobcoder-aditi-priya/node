function abc(){
    return "Hello World!"
}
async function kitchen(){
    try{    
        console.log("try start")
        let a = await abc();
        console.log(a)
    }
    catch(error){
       console.log("abc does not exist", error)
    }
    finally{
       console.log("Runs code anyways")
    }
 }
 kitchen() 
















// const myPromise = new Promise((res, rej) => {
//     let num = 03
//     setTimeout(() => {
//         if (!num) {
//             res(console.log(" -promise resolved !"))
//         }else{
//             rej(" -promise rejected !")
//         }
//     }, 4000)
// })

// myPromise
//     .then(() => {return "Hello World!"})
//     .then((temp) => {return temp+"Hello You!"})
//     .then( temp => console.log(temp))
//     .catch((err) => { console.log(err) })
//     .finally(console.log(" -finally block"));

// console.log(myPromise)