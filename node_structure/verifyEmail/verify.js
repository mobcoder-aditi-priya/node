const express=require('express')
var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://userad:12345@localhost:27017/demo";

const app=express()


app.get("/:token",(req,res)=>{
    MongoClient.connect(url, function(err, db) {
        if (err) throw err;
        var dbo = db.db("demo");
        let query={verificationToken:req.params.token};
        console.log(req.params.token)
        dbo.collection("users").findOne(query, function(err, result) {
          if (err) throw err;
          else if(result===null){
              res.write("Invalid Token !!");
              res.end();
          }else {
            dbo.collection('users').updateOne(query, { $set: { isVerified: 1 } }, function (err, result) {
                if (err) throw err;
                else {
                    res.write("User Verified !");
                    res.end();
                }
            })
            console.log(result,"Verified Now !!")
          }
        });
      });
  }).listen(3001);
  

