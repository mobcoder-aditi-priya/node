const usrRoutr = require("express").Router();
const resHndlr = require("../../../responseHandler");
const middleware = require("../../../middleware");
const constants = require("../../../constant");
const usrFacade = require("./userFacade");
const validators = require("./userValidators");
const uploadToS3 = require('../../../service/uploadToS3');
usrRoutr.route("/signup")
    .post([middleware.multer.single('fileupload'), validators.validateSignup], function (req, res) {
        return uploadToS3.uploadFile(req.file)
            .then(function (fileupload) {
                return fileupload;
            }).then(function (bucket) {
                let { email,password, name, dob, gender, address, phone } = req.body;
                //console.log(bucket,"bbbbbbbbbbbbbbbbbbbbbbbbbbb");
                let fileupload = bucket.Location;
                // console.log(fileupload);
                return usrFacade.signUp({
                    email,password, name, dob, gender, address, phone,fileupload
                }).then(function (result) {
                    resHndlr.sendSuccess(res, result, req);
                }).catch(function (err) {
                    resHndlr.sendError(res, err, req);
                })
            });
    });

usrRoutr.route("/login")
    .post([validators.validateLogin], function (req, res) {
        let { email, password } = req.body;
        //console.log(req.body,"lllllllllllllllllllllllllll")
        usrFacade.login({
            email, password
        }).then(function (result) {
            resHndlr.sendSuccess(res, result, req);
        }).catch(function (err) {
            resHndlr.sendError(res, err, req);
    })
});

usrRoutr.route("/social-login")
    .post([validators.validateSocial], function (req, res) {
        let { deviceToken, deviceID, deviceTypeID, currentVersion, socialType, socialId, name, username, email, dob, gender, fileupload } = req.body;

        usrFacade.socialLogin({
            deviceToken, deviceID, deviceTypeID, currentVersion, socialType, socialId, name, username, email, dob, gender, fileupload
        }).then(function (result) {
            resHndlr.sendSuccess(res, result, req);
        }).catch(function (err) {
            resHndlr.sendError(res, err, req);
        })
    });

usrRoutr.route("/guest-login")
    .post([middleware.multer.single('fileupload'), validators.validateGuest], function (req, res) {
        let { deviceToken, deviceID, deviceTypeID, currentVersion, name, username, location } = req.body;
        var fileupload;
        if (req.file) {
            fileupload = req.file.filename;
        }
        usrFacade.guestLogin({
            deviceToken, deviceID, deviceTypeID, currentVersion, name, username, fileupload
        }).then(function (result) {
            resHndlr.sendSuccess(res, result, req);
        }).catch(function (err) {
            resHndlr.sendError(res, err, req);
        })
    });

    usrRoutr.route("/logout")
    .post([], function (req, res) {               
        console.log(req.headers.accesstoken,"aaaaaaaaaaaaaaaaaaaaaaaaaa")
        let  accessToken  = req.headers.accesstoken
        usrFacade.logout({
           accessToken
        }).then(function (result) {
            resHndlr.sendSuccess(res, result, req);
        }).catch(function (err) {
            resHndlr.sendError(res, err, req);
        })
});


module.exports = usrRoutr;
