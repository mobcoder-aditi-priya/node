"use strict";

//========================== Load Modules Start =======================

//========================== Load external modules ====================
// Load user service
var _ = require("lodash");
var Promise = require("bluebird");
const nodemailer = require('../../../service/nodemailer_email');
//========================== Load internal modules ====================

const usrService = require('./userService');
const userMapper = require('./userMapper');


const appUtils = require('../../../appUtils');
const redisClient = require("../../../redisClient/init");
const exceptions = require("../../../customException");

const jwtHandler=require("../../../redisClient/jwtHandler");

//========================== Load Modules End ==============================================

function signUp(loginInfo) {
    return usrService.getUserByEmailId(loginInfo)
        .then((user) => {
            this.user = user
            if (user === null) {
                return usrService.signUp(loginInfo)

            } else {
                throw { success: false, msg: 'User already exists' }
            }
        })
        .then(function (user) {
            const payload = {
                to: user.email,
                from: 'aditi2021priya@gmail.com',
                subject: 'Signed Up successfully !',
                //token: this.user.ewt,
                template: `<h2>Hello ${user.name}..</h2>    
                            <h5>You have signed up sucessfully.Your token is ${user.verificationToken} <br>
                            Kindly verify the mail by clicking the following link</h5>
                            <a href=http://localhost:3001/${user.verificationToken}> Click here</a>`,
            }
            nodemailer.sendEmail(payload)
            return loginInfo
        })
        .then((user) => {
            return userMapper.loginMapping({ user: user });
        })
}

function socialLogin(loginInfo) {
    return usrService.isSocialIdExist(loginInfo)
        .bind({})
        .then(function (user) {
            this.user = user;
            if (user) {
                return usrService.socialLogin(user, loginInfo)
            } else {
                return usrService.socialSignUp(loginInfo)
            }
        })
        .then(function (response) {
            if (response) {
                this.user = response;
                var tokenObj = buildUserTokenGenObj(response);
                return jwtHandler.generateUserToken(tokenObj)
            } else {
                throw exceptions.incorrectPass();
            }
        })
        .then(function (response) {

            this.jwt = response;
            let redisObj = appUtils.createRedisValueObject({ user: this.user });
            redisClient.setValue(response, JSON.stringify(redisObj));
            if (this.user) {
                return userMapper.loginMapping({
                    user: this.user,
                    jwt: this.jwt
                });
            }

        })
}

function guestLogin(loginInfo) {
    return usrService.getUserByDeviceId(loginInfo)
        .bind({})
        .then(function (user) {
            this.user = user
            if (this.user) {
                let query = {};
                query.deviceID = loginInfo.deviceID;
                var update = {};
                if (loginInfo.deviceToken) {
                    update.deviceToken = loginInfo.deviceToken;
                }
                if (loginInfo.name) {
                    update.name = loginInfo.name;
                }
                if (loginInfo.username) {
                    update.username = loginInfo.username;
                }
                if (loginInfo.profileImage) {
                    update.profileImage = loginInfo.profileImage;
                }
                update.deviceTypeID = loginInfo.deviceTypeID;
                return usrService.updateUser(query, update)
                    .then(function (response) {
                        if (response) {
                            return response;
                        } else {
                            throw exceptions.intrnlSrvrErr();
                        }
                    })
            } else {
                return usrService.signUp(loginInfo)
                    .then(function (response) {
                        if (response) {
                            return response;
                        } else {
                            throw exceptions.intrnlSrvrErr();
                        }
                    })
            }
        })
        .then(function (response) {
            if (response) {
                this.user = response;
                var tokenObj = buildUserTokenGenObj(response);
                return jwtHandler.generateUserToken(tokenObj)
            } else {
                throw exceptions.incorrectPass();
            }
        })
        .then(function (response) {
            if (this.user) {
                this.jwt = response;
                let redisObj = appUtils.createRedisValueObject({ user: this.user });
                redisClient.setValue(response, JSON.stringify(redisObj));
                if (this.user) {
                    return userMapper.loginMapping({
                        user: this.user,
                        jwt: this.jwt,
                        currentVersion: loginInfo.currentVersion,
                    });
                }
            }
        })

}


function login(loginInfo) {
    return usrService.login(loginInfo)
        .bind({})
        .then(function (user) {
            //console.log(user,"jjjjjjjjjjjjjjjjjjjjj")
            if (user === null) {
                throw { success: false, 'message': 'User does not exist' }
            } else if (user.isVerified === 0) {
                throw { success: false, 'message': 'User is not Verified' }
            } else if (user.isVerified === 1) {
                //console.log(user,"kkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk")
                return user
            }
        })
        .then(function (response) {
            //console.log(response);
            if (response) {
                this.user = response;
                var tokenObj = buildUserTokenGenObj(response);
                return jwtHandler.generateUserToken(tokenObj)
            }
        })
        .then(function (response) {
            let jwt = response;
            let redisObj = appUtils.createRedisValueObject(JSON.stringify({ user: jwt }));
            redisClient.setValue(response, JSON.stringify(redisObj));
            {
                return userMapper.loginMapping({
                    user: user,
                    jwt: jwt
                });
            }

        })
}


function buildUserTokenGenObj(user) {
    var userObj = {};
    userObj.userId = user._id;
    userObj.email = user.email;
    userObj.name = user.name;
    //console.log(userObj, 'uuuuuuuuuuuooooooooooooo')
    return userObj;
}

function logout(accessToken) {
    //console.log(accessToken.accessToken,"facade>>>>>>>>>>>")
    return usrService.logout(accessToken)
}


//========================== Export Module Start ==============================

module.exports = {
    signUp,
    guestLogin,
    login,
    socialLogin,
    logout
};

//========================== Export Module End ===============================signUp