//string after number
console.log(2+ +2+'5'+6)
//456
console.log(2+'2'+5+6)
//2256

//string after string
console.log(2+'2'+'5'+'6')
//456


//string after nothing && previous result as number
console.log(2+ +2+5+ +'6')
//15
//string after nothing && previous result as string
console.log(2+ +2+'5'+ +'6')
//456
console.log(2+ +'2'+5+'6')
//96



// string after number/string =>concatenates
// string after nothing =>takes action on basis of previous datatype