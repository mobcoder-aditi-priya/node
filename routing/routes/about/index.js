const express=require("express")

const inAboutRouter=require('./inAbout');
const router=express.Router();

router.get('/',(req,res) =>{
    res.json(['sectionInside','about']);
});

router.use('./inAbout',inAboutRouter);

module.exports=router;