const express=require("express")

const router=express.Router();

const aboutRouter=require('./about');
router.use('./about',aboutRouter);

router.get('/',(req,res) =>{
    res.json(['on','mainPage'])
    res.send("Welcome !!")
});

// router.get('/contact',(req,res) =>{
//     res.send("Welcome to contact section!!")
// });

// router.get('/blogs',(req,res) =>{
//     res.send("Welcome to blogs section!!")
// });


// router.get('/about/achievements',(req,res) =>{
//     res.send("Welcome to achievements section of about !!")
// });

// router.get('/about/subscription',(req,res) =>{
//     res.send("Welcome to subscription section of about !!")
// });

module.exports=router;