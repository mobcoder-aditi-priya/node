const express=require('express');
const appln=express();

const urlCheck=function(req,res,next){
    console.log("Current route is ",req.originalUrl);
    next();
}
appln.use(urlCheck)


appln.get('/',function(req,res){
    res.send("hello")
});
appln.get('/about',function(req,res){
    res.send("welcome to about page ")
});
appln.get('/contact',function(req,res){
    res.send("welcome to contact us page")
});
appln.get('/blogs',function(req,res){
    res.send("welcome to blogs page")
});
appln.post('/subscription',function(req,res){
    res.send("welcome to subscription page post method")
});
appln.put('/subscription',function(req,res){
    res.send("welcome to subscription page put method")
});
appln.delete('/subscription',function(req,res){
    res.send("welcome to subscription page delete method")
});

appln.listen(8000);