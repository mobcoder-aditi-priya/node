const http=require("http");
const fs=require("fs");

http.createServer( (req,res) =>{
    if(req.url=='/'){
        fs.readFile('index.html', function (err, data) {
            res.writeHead(200, { 'Content-Type': 'text/html' });
            res.write(data);
            return res.end();
        })
    }else if(req.url=='/home'){
        fs.readFile('home.html', function (err, data) {
            res.writeHead(200, { 'Content-Type': 'text/html' });
            res.write(data);
            return res.end();
        })
    }else if(req.url=='/contact'){
        fs.readFile('contactUs.html', function (err, data) {
            res.writeHead(200, { 'Content-Type': 'text/html' });
            res.write(data);
            return res.end();
        })
    }else if(req.url=='/blogs'){
        fs.readFile('blogs.html', function (err, data) {
            res.writeHead(200, { 'Content-Type': 'text/html' });
            res.write(data);
            return res.end();
        })
    }
    else{
        res.writeHead(404, {"Content-type": "text/html"});
        res.end("<h3>Page not found :( </h3>");
    }
}).listen(8080)
