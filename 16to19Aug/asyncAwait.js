

function stay3sec() {
    return new Promise(resolve => {
      setTimeout(() => {
        resolve('Thanks for waiting!!');
      }, 3000);
    });
  }
  
  async function asyncCall() {
    console.log('calling');
    const result = await stay3sec();
    console.log(result);
  }
  
  asyncCall();
  