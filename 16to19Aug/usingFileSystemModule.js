//Reading a HTML file

var http = require('http');
var fs = require('fs');
http.createServer(function (req, res) {
  fs.readFile('index.html', function(err, data) {
    res.writeHead(200, {'Content-Type': 'text/html'});
    res.write(data);
    return res.end();
  });
}).listen(8080);


//creating and appending 

fs.appendFile('createdViaFS()module.txt', 'Hey, if file not exist it will create one of specified name and if exist ,content added to existing file!', function (err) {
  if (err) throw err;
  console.log('Saved! / Updated!1');

});

fs.appendFile('createdToPerformUnlink().txt', 'Hey, this file is created to perform unlink operation', function (err) {
    if (err) throw err;
    console.log('Saved! / Updated!11');
  
});


fs.appendFile('Random.txt', 'Hey, this file is created to perform rename operation!', function (err) {
    if (err) throw err;
    console.log('Saved! / Updated!111');
  
});



//opening writing mode of a file

fs.open('createdViaFS()module.txt', 'w', function (err, file) {
    if (err) throw err;
    console.log('Saved!');
});


//Writing in a file (replacing if any content exists in the file)

fs.writeFile('replace.txt', 'Replace any content if present with this content in this file if not exist it will create a file with this content', function (err) {
  if (err) throw err;
  console.log('Saved! / Replaced!');
});


//deleting a file

fs.unlink('createdToPerformUnlink().txt', function (err) {
    if (err) throw err;
    console.log('File deleted!');
  });


//Renaming Files

fs.rename('Random.txt', 'Renamed.txt', function (err) {
  if (err) throw err;
  console.log('File Renamed!');
});
