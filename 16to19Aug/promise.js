

let myPromise = new Promise(function(resolved, rejected) {
  let x = 7698;

  if (x) {
    resolved("Valid Input");
  } else {
    rejected("Invalid Input");
  }
});

myPromise.then(
  function(value) {console.log(value);},
  function(error) {console.log(error);}
);