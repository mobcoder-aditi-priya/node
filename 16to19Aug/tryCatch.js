const { randomBytes } = require("crypto");

try{
    console.log("reached inside try block")
    randomfunc();
}
catch(err){
    console.log("reached inside catch block")
    console.log(err);
}
finally{
    console.log("reached inside finally block");
    console.log("it will be executed always");
    let arr=["gd","",null,undefined,3945809];
    arr.forEach(element => {
        console.log(element)
    });
    
}